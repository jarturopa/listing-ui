import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { ApolloProvider } from '@apollo/react-hooks';

import {
  Home,
  Host,
  Listing,
  Listings,
  User,
  NotFound
} from './sections';
import reportWebVitals from './reportWebVitals';

import './styles/index.css';

const client = new ApolloClient({
  uri: '/api',
  cache: new InMemoryCache()
});

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/host' element={<Host />} />
        <Route path='/listing/:id' element={<Listing />} />
        <Route path='/listings/:location?' element={<Listings />} />
        <Route path='/user/:id' element={<User />} />
        <Route element={<NotFound />} />
      </Routes>
    </Router>
  );
};

root.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
